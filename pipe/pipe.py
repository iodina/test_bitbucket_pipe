import os

import telegram


my_token = os.getenv("TELEGRAM_TOCKEN")
default_message = "You're a very nice person"
chat_id = os.getenv("TELEGRAM_USER_CHAT_ID")


class MyPipe:

    def __init__(self, name):
        self.name = name

    def run(self):
        if self.name == "Terminator":
            raise Exception(f"Unexpected error occured {self.name}")
        print(f"My name is {self.name}")
        print(f"Hello World!")


class TelegramBotPipe(MyPipe):

    def send(self, chat_id, token, message=default_message):
        """
        Send a mensage to a telegram user specified on chatId
        chat_id must be a number!
        """
        print("I'm sending message to Telegram...")
        bot = telegram.Bot(token=token)
        bot.sendMessage(chat_id=chat_id, text=message)


if __name__ == "__main__":
    pipe = MyPipe(os.getenv("NAME", "MY_PRECIOUS_PIPE"))
    pipe.run()
    version = os.getenv("PIPE_VERSION_UPLOADED", "unknown")
    message = f"Hi, Mariia, new version {version} has been built and uploaded"
    TelegramBotPipe(name="Telegram bot").send(chat_id=chat_id, token=my_token, message=message)
