import pytest
from pipe.pipe import MyPipe


class TestMyPipe:

    @pytest.mark.parametrize("name", [
        "My Pipe",
    ])
    def test_run_success(self, capsys, name):
        my_pipe = MyPipe(name)
        my_pipe.run()
        captured = capsys.readouterr()
        assert name in captured.out
        assert "Hello World!" in captured.out

    @pytest.mark.parametrize("name", [
        "Terminator",
    ])
    def test_run_failure(self, capsys, name):
        my_pipe = MyPipe(name)
        with pytest.raises(Exception):
            my_pipe.run()
            captured = capsys.readouterr()
            assert name in captured.err
