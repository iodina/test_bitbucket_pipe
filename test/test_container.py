import os
import subprocess


docker_image = 'bitbucketpipelines/test_bitbucket_pipe:' + os.getenv('BITBUCKET_BUILD_NUMBER', "0.0.0")


def _docker_build():
    args = [
        'docker',
        'build',
        '-t',
        docker_image,
        '.',
    ]
    subprocess.run(args, check=True)


def setup():
    _docker_build()


def test_docker_run_success():
    test_name = "beautiful pipe"
    args = [
        'docker',
        'run',
        '-e', f'NAME={test_name}',
        docker_image,
    ]

    result = subprocess.run(args, check=False, text=True, capture_output=True)
    assert 0 == result.returncode
    assert test_name in result.stdout


def test_docker_run_failure():
    test_name_error = "Terminator"
    args = [
        'docker',
        'run',
        '-e', f'NAME={test_name_error}',
        docker_image,
    ]

    result = subprocess.run(args, check=False, text=True, capture_output=True)
    assert 1 == result.returncode
    assert test_name_error in result.stderr
